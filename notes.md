2.1 Для того, щоб додати нове домашнє завдання в новий репозиторій на Gitlab маємо зробити наступні кроки:
  - Створюємо новий репозиторій на сайті Gitlab.com.
  - На комп'ютері вибираємо місце, де зберігатиметься проект. Натискаємо на цю папку (Linex) або відкриваємо її та натискаємо (Windows) і відкриваємо термінал або git bash.
  - Клонуємо новий репозиторій на свій комп'ютер за допомогою команди git clone з посиланням на новий репозиторій.
  - Якщо система запитує ім'я користувача та пароль - вводимо своє ім'я користувача та свій пароль.
  - Перебуваючи в папці з проектом, пишемо команду git add . для того, щоб додати усі змінені файли до комміту.
  - Потім пишемо команду git commit -m "Initial commit" для того, щоб зберегти всі раніше зроблені зміни у локальному репозиторії.
  - Далі - git push -u origin master. Ця команда надсилає наш коміт на віддалений репозиторій.
  - Якщо система запитує ім'я користувача та пароль - вводимо своє ім'я користувача та свій пароль.
  - Оновлюєємо проект на Gitlab - бачимо, що остання версія файлів, які знаходяться на комп'ютері в нашій папці - тепер є на Gitlab.com.

2.2 Для того, щоб додати існуюче домашнє завдання в новий репозиторій на Gitlab маємо зробити наступні кроки:
  - Створюємо новий репозиторій на сайті Gitlab.com.
  - Заходимо в папку, де зберігається наш проект. Відкриваємо термінал у цій папці (Windows).
  - Пишемо команду git init для того, щоб створити в цій папці локальний репозиторій і працювати тут.
  - Пишемо команду git remote add origin з посиланням на новий репозиторій для того, щоб синхронізувати папку на комп'ютері з новим репозиторієм на Gitlab.com та відправляти туди всі зміни в коді.
  - Перебуваючи в папці з проектом, пишемо команду git add . для того, щоб додати усі змінені файли до комміту.
  - Потім пишемо команду git commit -m "Initial commit" для того, щоб зберегти всі раніше зроблені зміни у локальному репозиторії.
  - Далі - git push -u origin master. Ця команда надсилає наш коміт на віддалений репозиторій.
  - Якщо система запитує ім'я користувача та пароль - вводимо своє ім'я користувача та свій пароль.
  - Оновлюєємо проект на Gitlab - бачимо, що остання версія файлів, які знаходяться на комп'ютері в нашій папці - тепер є на Gitlab.com.